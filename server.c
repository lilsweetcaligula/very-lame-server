#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "server.h"

#define LISTEN_BACKLOG 30

int server_start(server_t *serv, const char *port) {
  assert(serv);

  int err = 0;
  struct addrinfo hints = { 0 };

  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  err = getaddrinfo(NULL, port, &hints, &serv->addrinfo);

  if (err) {
    return err;
  }

  serv->sock = socket(serv->addrinfo->ai_family,
    serv->addrinfo->ai_socktype, serv->addrinfo->ai_protocol);

  if (serv->sock == -1) return -1;

  err = bind(serv->sock, serv->addrinfo->ai_addr,
    serv->addrinfo->ai_addrlen);

  if (err) {
    close(serv->sock);
    serv->sock = -1;
    return -1;
  }

  if (serv->sock == -1) {
    return -1;
  }

  err = listen(serv->sock, LISTEN_BACKLOG);

  if (err) {
    return err;
  }

  return 0;
}

int server_dispose(server_t *serv) {
  assert(serv);

  int err = 0;

  err = close(serv->sock);
  serv->sock = -1;

  freeaddrinfo(serv->addrinfo);
  serv->addrinfo = NULL;

  return err;
}

const struct sockaddr_in *server_addr(server_t *serv) {
  assert(serv);
  assert(serv->addrinfo);

  return (struct sockaddr_in*) serv->addrinfo->ai_addr;
}

