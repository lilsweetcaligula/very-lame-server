#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "debug_logger.h"
#include "errors.h"
#include "request.h"
#include "headers.h"
#include "client.h"

#define CHUNK_READ_SZ 255
#define LINE "\r\n"
#define SPACE " "
#define BLANK_LINE "\r\n\r\n"

int request_read(request_t *req, client_t *cli) {
  assert(req);
  assert(cli);
  assert(cli->sock);

  int err = 0;
  ssize_t num_bytes = 0;

  size_t buf_sz = 0;
  char buf[32 * CHUNK_READ_SZ] = { 0 };

  char *headers_end = NULL;

  while (!headers_end) {
    if (buf_sz + CHUNK_READ_SZ + 1 > sizeof(buf)) {
      return ERR_REQUEST_TOO_LARGE;
    }

    debug_log("reading the request chunk");

    num_bytes = read(cli->sock, &buf[buf_sz], CHUNK_READ_SZ);

    if (num_bytes < 0) {
      return num_bytes;
    }

    debug_log("the request chunk has been read: %d", num_bytes);

    buf_sz += num_bytes;
    buf[buf_sz] = '\0';

    if (num_bytes == 0) {
      debug_log("received an empty request");

      if (buf_sz == 0) {
        return ERR_EMPTY_REQUEST;
      }
    }

    debug_log("\nthe buffer now is:\n%s", buf);

    headers_end = strstr(buf, BLANK_LINE);
  }


  const char *const req_begin = buf;
  const char *const req_end = strstr(buf, LINE);

  if (!req_end) {
    return ERR_MALFORMED_REQUEST;
  }


  const char *const method_begin = buf;
  const char *const method_end = strstr(buf, SPACE);

  if (!method_end) {
    return ERR_MALFORMED_REQUEST;
  }

  const size_t method_len = method_end - method_begin;
  char *method = calloc(method_len + 1, sizeof(*method));

  if (!method) {
    return ERR_MALFORMED_REQUEST;
  }

  strncpy(method, method_begin, method_len);


  const char *const path_begin = method_end + strlen(SPACE);
  const char *const path_end = strstr(path_begin, SPACE);

  if (!path_end) {
    free(method);

    return ERR_MALFORMED_REQUEST;
  }

  const size_t path_len = path_end - path_begin;
  char *path = calloc(path_len + 1, sizeof(*path));

  if (!path) {
    free(method);

    return ERR_MALFORMED_REQUEST;
  }

  strncpy(path, path_begin, path_len);


  const char *const headers_begin = req_end + strlen(LINE);

  headers_t headers = { 0 };

  err = headers_from_chunk(&headers, headers_begin,
    1 + headers_end - headers_begin);

  if (err) {
    free(method);
    free(path);
    headers_dispose(&headers);

    return err;
  }

  const char *const content_len_header = headers_get(&headers,
    "content-length");

  if (content_len_header) {
    const size_t content_len = strtoul(content_len_header, NULL, 10);

    if (errno) {
      free(method);
      free(path);
      headers_dispose(&headers);

      return errno;
    }

    const char *const partial_body = headers_end + strlen(BLANK_LINE);
    char *body = calloc(1 + content_len, sizeof(*body));

    if (!body) {
      free(method);
      free(path);
      headers_dispose(&headers);

      return err;
    }

    const size_t partial_body_len = strlen(partial_body);

    strcpy(body, partial_body);

    if (content_len > partial_body_len) {
      char *body_end = &body[partial_body_len];

      const ssize_t to_read = content_len - partial_body_len;
      ssize_t remaining = to_read;

      while (remaining > 0) {
        const ssize_t num_bytes = read(cli->sock, body_end, to_read);

        if (num_bytes < 0) {
          free(body);
          free(method);
          free(path);
          headers_dispose(&headers);

          return num_bytes;
        }

        body_end += num_bytes;
        remaining -= num_bytes;
      }
    }

    req->body = body;
  }

  req->headers = headers;
  req->method = method;
  req->path = path;


  return ERR_OK;
}

void request_dispose(request_t *req) {
  assert(req);

  headers_dispose(&req->headers);

  free(req->method); req->method = NULL;
  free(req->path); req->path = NULL;

  if (req->body) {
    free(req->body); req->body = NULL;
  }
}

int request_fprint(request_t* req, FILE* stream) {
  assert(req);
  assert(stream);

  char *headers_str = headers_to_string(&req->headers);

  if (!headers_str) {
    return errno;
  }

  fprintf(stream, "%s %s HTTP/1.1\r\n%s\r\n%s\n", req->method, req->path,
    headers_str, req->body ? req->body : "");

  free(headers_str);

  return errno;
}

