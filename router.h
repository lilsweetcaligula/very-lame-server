#ifndef ROUTER_H_
#define ROUTER_H_

#include "request.h"

int matching_route(const request_t *req, const char *method, const char *path);

#endif /* ROUTER_H_ */
