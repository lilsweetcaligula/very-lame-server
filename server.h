#ifndef SERVER_H_
#define SERVER_H_


typedef struct {
  int sock;
  struct addrinfo *addrinfo;
} server_t;

int server_start(server_t *serv, const char *port);

int server_dispose(server_t *serv);

const struct sockaddr_in *server_addr(server_t *serv);


#endif /* SERVER_H_ */
