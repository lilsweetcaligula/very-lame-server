const Http = require('http')
const Path = require('path')
const Fs = require('fs')
const { requestedHost, requestedPort } = require('./utils.js')

const server = new Http.Server((req, res) => {
  console.log('Incoming request for', req.url)

  const view_path = Path.join(__dirname, './index.html')

  console.log('Serving', view_path)

  return Fs.readFile(view_path, (err, data) => {
    if (err) {
      console.error(err)

      res.writeHead(500)
      res.end()

      return
    }


    console.log('About to respond with:', data.toString('utf-8'))

    res.writeHead(200)
    res.write(data)
    res.end()
  })
})


const port = requestedPort(process.argv)
const host = requestedHost(process.argv)

server.listen(port, host, () => {
  console.log('The server is listening at: %j', server.address())
})
