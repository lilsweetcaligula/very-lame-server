const Http = require('http')
const Express = require('express')
const { requestedHost, requestedPort } = require('./utils.js')

const app = Express()

app.get('/', (req, res) => {
  return res.status(404).json({ message: 'hello world!' })
})

const server = new Http.Server(app)


const port = requestedPort(process.argv)
const host = requestedHost(process.argv)

server.listen(port, host, () => {
  console.log('The server is listening at: %j', server.address())
})
