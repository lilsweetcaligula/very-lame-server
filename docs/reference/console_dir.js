const app = () => {
  let content = Buffer.from('')

  process.stdin.on('data', data => {
    content = Buffer.concat([data, content])
  })

  process.stdin.once('end', () => {
    console.dir(content.toString('utf-8'))
  })
}

app()
