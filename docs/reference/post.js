const Http = require('http')
const Express = require('express')
const { requestedHost, requestedPort } = require('./utils.js')

const app = Express()

const withRawBody = () => (req, res, buf, enc) => {
  if (buf && buf.length) {
    req.rawBody = buf.toString(enc || 'utf8');
  }
}

app.use(Express.raw({ verify: withRawBody(), type: '*/*' }))

app.post('/', (req, res) => {
  console.dir(req.rawHeaders)
  return req.pipe(res)
})

const server = new Http.Server(app)


const port = requestedPort(process.argv)
const host = requestedHost(process.argv)

server.listen(port, host, () => {
  console.log('The server is listening at: %j', server.address())
})
