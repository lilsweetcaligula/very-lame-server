const DEFAULT_HOST = '0.0.0.0'
const DEFAULT_PORT = 3000

exports.requestedHost = argv =>
  argv.length < 4 ? DEFAULT_HOST
                  : argv[2]

exports.requestedPort = argv =>
  argv.length < 3 ? DEFAULT_PORT
                  : argv[2]

