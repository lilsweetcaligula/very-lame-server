const Http = require('http')
const Path = require('path')
const Express = require('express')
const { requestedHost, requestedPort } = require('./utils.js')

const app = Express()

app.get('/', (req, res) => {
  return res.sendFile(Path.join(__dirname, 'index.html'))
})

const server = new Http.Server(app)

const port = requestedPort(process.argv)
const host = requestedHost(process.argv)

server.listen(port, host, () => {
  console.log('The server is listening at: %j', server.address())
})
