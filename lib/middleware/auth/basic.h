#ifndef MD_AUTH_BASIC_HD
#define MD_AUTH_BASIC_HD

#include "../../../router.h"

int require_basic_auth(
  const request_t *req,
  client_t *cli,
  const char *user,
  const char *pass,
  int *out_ok
);

#endif /* MD_AUTH_BASIC_HD */
