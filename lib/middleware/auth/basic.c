#include <stdlib.h>
#include <string.h>

#include "basic.h"

#include "../../../client.h"
#include "../../../headers.h"
#include "../../../request.h"
#include "../../../response.h"

#include "../../base64.h"
#include "../../cstr.h"

int require_basic_auth(
  const request_t *req,
  client_t *cli,
  const char *expected_user,
  const char *expected_pass,
  int *out_ok
) {
  assert(req);
  assert(cli);
  assert(expected_user);
  assert(expected_pass);
  assert(out_ok);

  *out_ok = 0;
  int err = 0;

  const char *const basic_auth = headers_get(&req->headers,
    "Authorization");

  if (basic_auth) {
    if (cstr_startswith_ci(basic_auth, "Basic")) {
      const char *const psep = strchr(basic_auth, ' ');

      if (psep) {
        const char *const base64_credentials = &psep[1];
        char *const credentials = base64_dec(base64_credentials);

        if (credentials) {
          const char *const user = credentials;
          const char *user_end = user;

          while (*user_end && *user_end != ':') user_end++;

          if (*user_end) {
            const char *const password = &user_end[1];
            const char *password_end = password;

            while (*password_end) password_end++;


            const size_t user_len = user_end - user;

            const int matches_user = user_len == strlen(expected_user) &&
              !strncmp(user, expected_user, user_len);


            const size_t password_len = password_end - password;

            const int matches_password = strlen(expected_pass) &&
              !strncmp(password, expected_pass, password_len);


            if (matches_user && matches_password) {
              free(credentials);
              *out_ok = 1;
              return 0;
            }
          }

          free(credentials);
        }
      }
    }
  }

  headers_t hd = { 0 };
  headers_init(&hd);

  err = headers_init(&hd);

  if (err) {
    return err;
  }

  err = headers_add(&hd, "WWW-Authenticate", "Basic");

  if (err) {
    headers_dispose(&hd);
    return err;
  }

  err = respond(cli, 401, &hd, NULL, 0);

  headers_dispose(&hd);

  return err;
}

