#ifndef BASE64_H
#define BASE64_H

#include <stdint.h>

char *base64_dec(const char *b64);
static void uint32_print_bytes(uint32_t x);
static uint32_t base64_sym2val(char sym);

#endif /* BASE64_H */
