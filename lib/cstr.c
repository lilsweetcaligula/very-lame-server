#include <assert.h>
#include <ctype.h>
#include "cstr.h"

int cstr_startswith_ci(const char *s, const char *prefix) {
  assert(s);
  assert(prefix);

  const char *pc = s, *pt = prefix;

  while (*pc && *pt) {
    if (tolower(*pc) != tolower(*pt)) {
      return 0;
    }

    pc++;
    pt++;
  }

  return 1;
}

