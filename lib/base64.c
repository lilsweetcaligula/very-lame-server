#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "base64.h"

#define BASE64_INVALID_SYM 64

static const char BASE64_ALPHABET[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                      "abcdefghijklmnopqrstuvwxyz"
                                      "0123456789+/=";

static void uint32_print_bytes(uint32_t x) {
  char bytes[4] = { 0 };
  
  memcpy(bytes, &x, sizeof(bytes));  
  printf("%d, %d, %d, %d\n", bytes[0], bytes[1], bytes[2], bytes[3]);
}

static uint32_t base64_sym2val(char sym) {
  const char *const psym = strchr(BASE64_ALPHABET, sym);
  
  if (!psym) {
    return BASE64_INVALID_SYM;
  }
  
  return (psym - BASE64_ALPHABET) % 64;
}

char *base64_dec(const char *b64) {
  assert(b64);
  
  char *const dec = calloc(1 + strlen(b64), sizeof(*dec));
  char *to = dec;
  
  if (!dec) {
    return NULL;
  }

  for (const char *pc = b64; *pc; pc += 4) {
    const char *const q_end = &pc[4];
    uint32_t bits = 0;
    
    for (const char *pt = pc; pt != q_end; pt++) {
      const uint32_t b64_val = base64_sym2val(*pt);

      if (b64_val == BASE64_INVALID_SYM) {
        free(dec);
        return NULL;
      }
      
      bits = (bits << 6) | b64_val;
    }
    
    const char *const from = (char*) &bits;
    
    memcpy(&to[0], &from[2], sizeof(char));
    memcpy(&to[1], &from[1], sizeof(char));
    memcpy(&to[2], &from[0], sizeof(char));
    
    to += 3;
  }
  
  return dec;
}

