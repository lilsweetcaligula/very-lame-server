#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "debug_logger.h"
#include "response.h"
#include "headers.h"
#include "client.h"
#include "utils.h"

int send_file(client_t *cli, const char *filepath, int code, headers_t *headers) {
  assert(cli);
  assert(cli->sock);
  assert(filepath);
  assert(headers);

  int err = 0;
  const int fd = open(filepath, O_RDONLY);

  if (fd < 0) {
    return fd;
  }

  static const size_t READ_CHUNK_SIZE = 32;

  size_t buf_sz = 0;
  size_t buf_capa = 0;
  char *buf = NULL;

  ssize_t num_bytes = 0;

  do {
    const size_t required_capa = 1 + buf_sz + READ_CHUNK_SIZE;
    char *new_buf = realloc(buf, required_capa);

    if (!new_buf) {
      free(buf);
      close(fd);

      return -1;
    }

    buf = new_buf;
    buf_capa = required_capa;

    num_bytes = read(fd, &buf[buf_sz], READ_CHUNK_SIZE);

    if (num_bytes < 0) {
      free(buf);
      close(fd);
      return num_bytes;
    }

    buf_sz += num_bytes;
  } while (num_bytes > 0);

  buf[buf_sz] = '\0';

  err = close(fd);

  if (err) {
    free(buf);
    return err;
  }

  debug_log("this is what the response body looks like");
  debug_log("%s", buf);

  err = headers_addul(headers, "Content-Length", buf_sz);

  if (err) {
    free(buf);
    return err;
  }

  err = respond(cli, 200, headers, buf, buf_sz);

  free(buf);

  if (err) {
    return err;
  }

  return 0;
}

int respond(
  client_t *cli,
  int code,
  const headers_t *headers,
  const char *body,
  size_t bodylen
) {
  assert(cli);
  assert(cli->sock);

  dprintf(cli->sock, "HTTP/1.1 %d %s\r\n", code, http_reason(code));
  
  if (headers) {
    struct hdnode *header = headers->nodes;

    while (header) {
      dprintf(cli->sock, "%s: %s\r\n", header->k, header->v); 
      header = header->next;
    }
  }

  dprintf(cli->sock, "\r\n");

  if (body) write(cli->sock, body, bodylen);

  return 0;
}

