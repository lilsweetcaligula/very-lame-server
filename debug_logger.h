#ifndef DEBUG_LOGGER_H_
#define DEBUG_LOGGER_H_


#include <stddef.h>

#define DEBUG_LOGGING_ENABLED 1

void debug_log(const char *fmt, ...);

void debug_nlog(const char *s, size_t len);


#endif /* DEBUG_LOGGER_H_ */
