#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>

#include "headers.h"

int headers_init(headers_t *headers) {
  assert(headers);

  headers->nodes = NULL;
  return 0;
}

int headers_add(headers_t *headers, const char *k, const char *v) {
  assert(headers);

  struct hdnode *const next = headers->nodes;
  struct hdnode *new_node = calloc(1, sizeof(*headers->nodes));

  if (!new_node) {
    return -1;
  }

  new_node->k = calloc(1 + strlen(k), sizeof(*k));

  if (!new_node->k) {
    free(new_node);
    return -1;
  }

  new_node->v = calloc(1 + strlen(v), sizeof(*v));

  if (!new_node->v) {
    free(new_node);
    free(new_node->k);
    return -1;
  }

  strcpy(new_node->k, k);
  strcpy(new_node->v, v);

  new_node->next = next;
  headers->nodes = new_node;

  return 0;
}

int headers_addul(headers_t* headers, const char *k, unsigned long x) {
  char v[32] = { 0 };

  if (snprintf(v, sizeof(v), "%ld", x) < 1) {
    return -1;
  }

  return headers_add(headers, k, v);
}

const char *headers_get(const headers_t *headers, const char *k) {
  assert(headers);

  struct hdnode *node = headers->nodes;

  while (node) {
    if (strcasecmp(node->k, k) == 0) {
      return node->v;
    }

    node = node->next;
  }

  return NULL;
}

bool headers_has(const headers_t *headers, const char *k) {
  return headers_get(headers, k) != NULL;
}

void headers_dispose(headers_t *headers) {
  assert(headers);

  struct hdnode *node = headers->nodes;

  while (node) {
    struct hdnode *next = node->next;

    free(node->k);
    free(node->v);
    free(node);

    node = next;
  }
}

char *headers_to_string(const headers_t *headers) {
  assert(headers);

  static const char *const SEP = ": ";
  static const size_t SEP_LEN = strlen(SEP);

  static const char *const CLRF = "\r\n";
  static const size_t CLRF_LEN = strlen(CLRF);

  size_t buf_sz = 0;
  size_t buf_capa = 4;
  char *buf = calloc(1 + buf_capa, sizeof(*buf));

  if (!buf) {
    return NULL;
  }

  struct hdnode *node = headers->nodes;

  while (node) {
    const char *const k = node->k;
    const char *const v = node->v;

    const size_t header_len = strlen(k) + strlen(v) + SEP_LEN + CLRF_LEN;

    if (buf_capa < buf_sz + header_len) {
      const size_t required_len = buf_sz + header_len + 1;

      const size_t new_capa = required_len > 2 * buf_capa
        ? required_len
        : 2 * buf_capa;

      char *new_buf = realloc(buf, new_capa);

      if (!new_buf) {
        free(buf);
        return NULL;
      }

      buf = new_buf;
      buf_capa = new_capa;
    }

    strcpy(&buf[buf_sz], k);
    buf_sz += strlen(k);

    strcpy(&buf[buf_sz], SEP);
    buf_sz += SEP_LEN;

    strcpy(&buf[buf_sz], v);
    buf_sz += strlen(v);

    strcpy(&buf[buf_sz], CLRF);
    buf_sz += CLRF_LEN;

    node = node->next;
  }

  return buf;
}

int headers_from_chunk(headers_t* headers, const char *chunk, size_t len) {
  assert(headers);
  assert(chunk);

  int err = 0;
  size_t i = 0;

  while (1) {
    const char *const k = &chunk[i];

    while (i < len && chunk[i] != ':') i++;

    if (i >= len) break;

    const char *const k_end = &chunk[i];
    const size_t k_len = k_end - k;

    i++;

    if (i >= len || chunk[i] != ' ') break;

    i++;

    const char *const v = &chunk[i];

    while (1) {
      while (i < len && chunk[i] != '\r') i++;

      if (i < len) {
        i++;

        if (i >= len) break;
        if (chunk[i] != '\n') continue;
      }

      break;
    }

    i++;

    const char *const v_end = &chunk[i - 2];
    const size_t v_len = v_end - v;


    char *k_buf = calloc(1 + k_len, sizeof(*k_buf));
    char *v_buf = calloc(1 + v_len, sizeof(*v_buf));

    if (!k_buf) {
      return -1;
    }

    if (!v_buf) {
      return -1;
    }

    strncpy(k_buf, k, k_len);
    strncpy(v_buf, v, v_len);

    err = headers_add(headers, k_buf, v_buf);

    if (err) {
      return err;
    }

    free(k_buf); k_buf = NULL;
    free(v_buf); v_buf = NULL;
  }

  return 0;
}

