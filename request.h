#ifndef REQUEST_H_
#define REQUEST_H_

#include <stdio.h>

#include "headers.h"
#include "client.h"

typedef struct {
  headers_t headers; // TODO: the headers must be allocated on the heap
  char *method;
  char *path;
  char *body;
} request_t;

int request_read(request_t*, client_t *cli);

void request_dispose(request_t*);

int request_fprint(request_t*, FILE*);

#endif /* REQUEST_H_ */
