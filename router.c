#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "debug_logger.h"
#include "router.h"
#include "request.h"

int matching_route(const request_t *req, const char *method, const char *path) {
  assert(req);
  assert(method);
  assert(path);

  static const char *ALL = "*";

  const int matches_method = !strcmp(req->method, method) || !strcmp(ALL, method);
  const int matches_path = !strcmp(req->path, path) || !strcmp(ALL, path);

  return matches_method && matches_path;
}

