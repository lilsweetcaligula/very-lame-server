#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "client.h"

int client_accept(client_t *cli, int sock) {
  socklen_t addrlen = 0;
  int cli_sock = accept(sock, &cli->addr, &addrlen);

  if (cli_sock == -1) {
    return -1;
  }

  cli->sock = cli_sock;

  return 0;
}

int client_dispose(client_t *cli) {
  assert(cli);

  int err = 0;

  err = close(cli->sock);
  cli->sock = -1;

  return err;
}

