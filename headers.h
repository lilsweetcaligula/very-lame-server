#ifndef HEADERS_H_
#define HEADERS_H_


#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

struct hdnode {
  char *k;
  char *v;
  struct hdnode *next;
};

typedef struct {
  struct hdnode *nodes;
} headers_t;

int headers_init(headers_t*);

int headers_add(headers_t*, const char *k, const char *v);

int headers_addul(headers_t*, const char *k, unsigned long x);

const char *headers_get(const headers_t*, const char *k);

bool headers_has(const headers_t*, const char *k);

void headers_dispose(headers_t*);

char *headers_to_string(const headers_t*);

int headers_from_chunk(headers_t*, const char*, size_t);


#endif /* HEADERS_H_ */
