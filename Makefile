CC=clang

server:
	$(CC) \
	  headers.h \
	  headers.c \
	  client.h \
	  client.c \
	  utils.c \
	  utils.h \
	  server.h \
	  server.c \
	  request.h \
	  request.c \
	  response.h \
	  response.c \
	  router.h \
	  router.c \
	  debug_logger.h \
	  debug_logger.c \
	  errors.h \
	  lib/base64.h \
	  lib/base64.c \
	  lib/cstr.h \
	  lib/cstr.c \
	  lib/middleware/auth/basic.h \
	  lib/middleware/auth/basic.c \
	  main.c

