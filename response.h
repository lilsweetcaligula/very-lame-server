#ifndef RESPONSE_H_
#define RESPONSE_H_

#include <stddef.h>

#include "client.h"
#include "headers.h"

int send_file(
  client_t *cli,
  const char *filepath,
  int code,
  headers_t *headers
);

int respond(
  client_t *cli,
  int code,
  const headers_t *headers,
  const char *body,
  size_t bodylen
);


#endif /* RESPONSE_H_ */
