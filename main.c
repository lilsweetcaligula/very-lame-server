#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "errors.h"
#include "server.h"
#include "client.h"
#include "headers.h"
#include "request.h"
#include "response.h"
#include "router.h"
#include "utils.h"
#include "debug_logger.h"

#include "lib/middleware/auth/basic.h"

#define DEFAULT_PORT "3000"
#define NUM_READ_RETRIES 3

#define BASIC_AUTH_USER "hax0r"
#define BASIC_AUTH_PASSWORD "1234"

int show_index(const request_t *req, client_t *cli, void *data);

int show_api_index(const request_t *req, client_t *cli, void *data);

int show_not_found(const request_t *req, client_t *cli, void *data);

int main(int argc, const char *argv[]) {
  int err = 0;
  server_t serv = { 0 };
  const char *const port = argc > 1 ? argv[1] : DEFAULT_PORT;


  err = server_start(&serv, port);

  if (err) {
    perror("Failed to start the server: ");
    return EXIT_FAILURE;
  }

  printf("Listening on port: %d\n", ntohs(server_addr(&serv)->sin_port));


  client_t cli = { 0 };

  while (1) {
    debug_log("waiting on client");

    err = client_accept(&cli, serv.sock);

    if (err) {
      perror("Failed to accept a client: ");
      return EXIT_FAILURE;
    }

    debug_log("client accepted");
    debug_log("preparing to read the request");

    request_t req = { 0 };

    for (int retry = 0; retry < NUM_READ_RETRIES; retry++) {
      debug_log("trying to read the request...");

      err = request_read(&req, &cli);

      if (err == ERR_EMPTY_REQUEST) {
        debug_log("received an empty request - ignoring...");
        continue;
      }

      if (err) {
        perror("Failed to read the request: ");
        return EXIT_FAILURE;
      }

      debug_log("the request has been read successfully");

      break;
    }

    if (!err) {
      debug_log("handling the request");

      request_fprint(&req, stdout); 

      if (matching_route(&req, "GET", "/")) {
        show_index(&req, &cli, NULL);
      } else if (matching_route(&req, "GET", "/api")) {
        show_api_index(&req, &cli, NULL);
      } else {
        show_not_found(&req, &cli, NULL);
      }

      request_dispose(&req);
    }
    
    err = client_dispose(&cli);

    if (err) {
      perror("Error occured on client disconnect: ");
      return EXIT_FAILURE;
    }
  }

  err = server_dispose(&serv);

  if (err) {
    perror("Error occured on server shutdown: ");
    return EXIT_FAILURE;
  }

  return 0;
}

int show_index(const request_t *req, client_t *cli, void *data) {
  assert(req);
  assert(cli);
  /* `data` can be null */

  debug_log("preparing the response");

  int err = 0;
  headers_t headers = { 0 };

  err = headers_init(&headers);

  if (err) {
    return err;
  }

  err = headers_add(&headers, "Content-Type", "text/html; charset=UTF-8");

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  err = send_file(cli, "assets/index.html", 200, &headers);

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  debug_log("response has been sent");

  headers_dispose(&headers);

  return err;
}

int show_api_index(const request_t *req, client_t *cli, void *data) {
  assert(req);
  assert(cli);
  /* `data` can be null */

  debug_log("preparing the response");

  int err = 0;
  int next = 0;

  err = require_basic_auth(req, cli, BASIC_AUTH_USER, BASIC_AUTH_PASSWORD, &next);

  if (err) {
    debug_log("basic auth returned an error code");
    return err;
  }

  if (!next) {
    debug_log("client failed authorization");
    return 0;
  }

  headers_t headers = { 0 };

  err = headers_init(&headers);

  if (err) {
    return err;
  }

  err = headers_add(&headers, "Content-Type", "text/html; charset=UTF-8");

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  err = send_file(cli, "assets/index.html", 200, &headers);

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  debug_log("response has been sent");

  headers_dispose(&headers);

  return err;
}

int show_not_found(const request_t *req, client_t *cli, void *data) {
  assert(req);
  assert(cli);
  /* `data` can be null */

  debug_log("preparing the response");

  int err = 0;
  headers_t headers = { 0 };

  err = headers_init(&headers);

  if (err) {
    return err;
  }

  err = headers_add(&headers, "Content-Type", "text/html; charset=UTF-8");

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  err = send_file(cli, "assets/not_found.html", 404, &headers);

  if (err) {
    headers_dispose(&headers);
    return err;
  }

  debug_log("response has been sent");

  headers_dispose(&headers);

  return err;
}

