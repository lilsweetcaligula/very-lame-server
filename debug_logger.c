#include "debug_logger.h"

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <assert.h>

void debug_log(const char *fmt, ...) {
  if (!(DEBUG_LOGGING_ENABLED)) {
    return;
  }

  va_list args;
  va_start(args, fmt);

  printf("DEBUG: ");
  vprintf(fmt, args);
  putchar('\n');

  va_end(args);
}

void debug_nlog(const char *s, size_t len) {
  assert(s);

  printf("DEBUG: ");

  for (size_t i = 0; i < len; i++) {
    putchar(s[i]);
  }

  putchar('\n');
}

