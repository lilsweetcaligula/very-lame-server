### Description

Welcome possibly the lamest server in existence - the Very Lame Server!
Warning - not production-ready, and probably never will be. It's just
a personal exploration project of mine to learn more about servers
through actually building one. Derp.

### Build

You will need `clang`, preferably version 6.0.0. You may check that you
have one on your Unix system by running:

```
$ clang --version
```

Clone the repo, `cd` into it, and run `make`.

### Running

Run the executable. If the port is omitted, the server defaults to 3000.
```
$ ./a.out 3000
```

That's all, now you can send HTTP request to it over the local network.
As of this writing the server only parses your request and greets you in
the best of HTTP traditions. Hopefully, later on I will implement some
sophistications.

### Bugs

It's likely very buggy, so if you stumble on any bugs, please feel free
to let me know through creating an issue.

