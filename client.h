#ifndef CLIENT_H_
#define CLIENT_H_


#include <sys/socket.h>

typedef struct {
  int sock;
  struct sockaddr addr;
} client_t;

int client_accept(client_t *cli, int sock);

int client_dispose(client_t *cli);


#endif /* CLIENT_H_ */
